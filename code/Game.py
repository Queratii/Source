import pygame
from Player import Joueur
from monstre import Goblin, Chauvesouris, Vere, Boss, Burger, Boss2, Boss3, Sniker, Neurone, Twit, Ramapant
from mise_en_liste import transfo


class Game:

    def __init__(self):
        self.is_playing = False
        self.all_player = pygame.sprite.Group()
        self.player = Joueur(self)
        self.all_player.add(self.player)
        self.all_monster = pygame.sprite.Group()
        self.pressed = {}
        self.spawn_monster(Goblin)
        self.tp = []
        self.n_mot = 0
        self.kill = False
        self.score = 0
        self.score_fin1 = 30
        self.score_fin_boss = 31
        self.score_fin2 = 61
        self.score_fin_boss2 = 62
        self.score_fin3 = 92
        self.score_fin_boss3 = 93
        self.font = pygame.font.SysFont('arial', 30)
        self.jump = False

    def update(self, screen, game):
        screen.blit(self.player.image, self.player.rect)

        self.player.update_health_bar(screen)

        if self.pressed.get(pygame.K_UP):
            self.jump = True
        if self.jump:
            self.player.jump()
            if self.player.rect.y == self.player.hauteurmax:
                self.jump = False
        self.player.app_gravity()

        for monster in game.all_monster:

            if self.kill:
                print(self.score)
                monster.remove()
                self.kill = False
                self.tp = []
                self.n_mot += 1
                if self.score <= self.score_fin1:
                    if self.score % 2 == 0:
                        if self.score == 10:
                            self.spawn_monster(Vere)
                        elif self.score == self.score_fin1:
                            self.spawn_monster(Boss)
                        else:
                            self.spawn_monster(Goblin)
                    else:
                        self.spawn_monster(Chauvesouris)

                elif self.score_fin2 >= self.score:

                    if self.score == self.score_fin2:
                        self.spawn_monster(Boss2)
                    elif self.score % 2 == 0:
                        self.spawn_monster(Sniker)
                    else:
                        self.spawn_monster(Burger)

                elif self.score_fin_boss3 > self.score > self.score_fin2:

                    if self.score == self.score_fin3:
                        self.spawn_monster(Boss3)
                    elif self.score % 2 == 0:
                        self.spawn_monster(Twit)
                    elif self.score == 73:
                        self.spawn_monster(Ramapant)
                    else:
                        self.spawn_monster(Neurone)

            else:
                monster.forward()
                monster.update_animation()
                monster.mot_to_kill(screen, self.lecture_mot("asset/mots.txt")[:-1])
        self.all_monster.draw(screen)
        if self.tp:
            self.aff_mot_tap(self.inv_transfo(self.tp), screen)
        if self.player.health == 0:
            self.score = 100

    def check_collision(self, sprite, group):
        return pygame.sprite.spritecollide(sprite, group, False)

    def spawn_monster(self, monster_name):
        self.all_monster.add(monster_name.__call__(self))

    def d_mtk(self):
        mtk = transfo(self.lecture_mot('asset/mots.txt')[:-1])
        return mtk

    def aff_mot_tap(self, letter, screen):
        aff_mot = self.font.render(letter, False, (255, 255, 255))
        screen.blit(aff_mot, (550, 550))

    def inv_transfo(self, tplist):
        chc = ""
        for ind in range(len(tplist)):
            chc += tplist[ind]
        return chc

    def do_kill(self):
        self.kill = False
        if self.get_touchpress() == self.d_mtk():
            self.kill = True
            self.score += 1

    def lecture_mot(self, fichier):
        fiche_mot = open(fichier, "r")
        for i in range(self.n_mot):
            fiche_mot.readline()
        t = fiche_mot.readline()
        fiche_mot.close()
        return t

    def get_touchpress(self):

        if self.pressed.get(pygame.K_a):
            self.tp.append("a")
        if self.pressed.get(pygame.K_b):
            self.tp.append("b")
        if self.pressed.get(pygame.K_c):
            self.tp.append("c")
        if self.pressed.get(pygame.K_d):
            self.tp.append("d")
        if self.pressed.get(pygame.K_e):
            self.tp.append("e")
        if self.pressed.get(pygame.K_f):
            self.tp.append("f")
        if self.pressed.get(pygame.K_g):
            self.tp.append("g")
        if self.pressed.get(pygame.K_h):
            self.tp.append("h")
        if self.pressed.get(pygame.K_i):
            self.tp.append("i")
        if self.pressed.get(pygame.K_j):
            self.tp.append("j")
        if self.pressed.get(pygame.K_k):
            self.tp.append("k")
        if self.pressed.get(pygame.K_l):
            self.tp.append("l")
        if self.pressed.get(pygame.K_m):
            self.tp.append("m")
        if self.pressed.get(pygame.K_n):
            self.tp.append("n")
        if self.pressed.get(pygame.K_o):
            self.tp.append("o")
        if self.pressed.get(pygame.K_p):
            self.tp.append("p")
        if self.pressed.get(pygame.K_q):
            self.tp.append("q")
        if self.pressed.get(pygame.K_r):
            self.tp.append("r")
        if self.pressed.get(pygame.K_s):
            self.tp.append("s")
        if self.pressed.get(pygame.K_t):
            self.tp.append("t")
        if self.pressed.get(pygame.K_u):
            self.tp.append("u")
        if self.pressed.get(pygame.K_v):
            self.tp.append("v")
        if self.pressed.get(pygame.K_w):
            self.tp.append("w")
        if self.pressed.get(pygame.K_x):
            self.tp.append("x")
        if self.pressed.get(pygame.K_y):
            self.tp.append("y")
        if self.pressed.get(pygame.K_z):
            self.tp.append("z")
        if self.pressed.get(pygame.K_SPACE):
            self.tp.append(" ")
        if self.pressed.get(pygame.K_6):
            self.tp.append("-")
        if self.pressed.get(pygame.K_BACKSPACE) or len(self.tp) > 50:
            self.tp = []

        return self.tp
