import pygame
import time


# classe joueur


class Joueur(pygame.sprite.Sprite):

    def __init__(self, game):
        self.game = game
        super().__init__()
        self.health = 30
        self.max_health = 30
        self.all_projectile = pygame.sprite.Group()
        self.image = pygame.image.load("asset/elya_main_ch.png")
        self.rect = self.image.get_rect()
        self.rect.x = 20
        self.rect.y = 400
        self.gravite = 10
        self.velojump = 16
        self.jumping = True
        self.hauteurmax = 250
        self.hsol = 410

    def damage(self, amount):
        if self.health - amount > amount:
            self.health -= amount

    def update_health_bar(self, surface):
        pygame.draw.rect(surface, (60, 63, 60), [self.rect.x + 0, self.rect.y + 110, self.max_health, 5])
        pygame.draw.rect(surface, (245, 253, 0), [self.rect.x + 0, self.rect.y + 110, self.health, 5])

    def jump(self):
        if self.jumping and self.rect.y >= self.hauteurmax:
            self.rect.y -= self.velojump
            self.peut_sauter()

    def app_gravity(self):
        if (not self.jumping) and self.rect.y <= self.hsol:
            self.rect.y += self.gravite
            self.peut_sauter()

    def peut_sauter(self):
        if self.rect.y <= self.hauteurmax:
            self.jumping = False
        if self.rect.y == self.hsol:
            self.jumping = True
