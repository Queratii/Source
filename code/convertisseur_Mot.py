# -*- coding: utf-8 -*-
"""
Created on Fri Mar  8 08:23:33 2024

@author: minh-vu.seng
"""


def lecture_mot(n):
    fiche_mot = open("mots", "r")
    for i in range(n):
        fiche_mot.readline()
    t = fiche_mot.readline()
    fiche_mot.close()
    return t


print(lecture_mot(0))
print(type(lecture_mot(0)))
