import pygame
import math
from Game import Game
# codée a l'aide de Graven developement

pygame.init()

clock = pygame.time.Clock()
screen_size = (1300, 650)
pygame.display.set_caption("Decrochage")
screen = pygame.display.set_mode(screen_size)
icon = pygame.image.load("asset/Logo.png")
pygame.display.set_icon(icon)

background = pygame.image.load("asset/menu.png")
background = pygame.transform.scale(background, screen_size)
background1 = pygame.image.load("asset/fond1.png")
background1 = pygame.transform.scale(background1, screen_size)
background2 = pygame.image.load("asset/fond2.png")
background2 = pygame.transform.scale(background2, screen_size)
background3 = pygame.image.load('asset/fond3.png')
background3 = pygame.transform.scale(background3, screen_size)
background_boss1 = pygame.image.load("asset/fond_boss1.png")
background_boss1 = pygame.transform.scale(background_boss1, screen_size)
background_boss2 = pygame.image.load("asset/fond_boss2.png")
background_boss2 = pygame.transform.scale(background_boss2, screen_size)
background_boss3 = pygame.image.load("asset/fond_boss3.png")
background_boss3 = pygame.transform.scale(background_boss3, screen_size)
background_fin = pygame.image.load("asset/fond-fin.png")
background_fin = pygame.transform.scale(background_fin, screen_size)


play_button = pygame.image.load("asset/play_button.png")
play_button = pygame.transform.scale(play_button, (200, 200))
play_button_rect = play_button.get_rect()
play_button_rect.x = math.ceil(screen.get_width() / 4)
play_button_rect.y = math.ceil(screen.get_height() / 2)

quit_button = pygame.image.load("asset/quit_button.png")
quit_button = pygame.transform.scale(quit_button, (200, 200))
quit_button_rect = quit_button.get_rect()
quit_button_rect.x = math.ceil(screen.get_width() / 2)
quit_button_rect.y = math.ceil(screen.get_height() / 2)

play = pygame.image.load('asset/play.png')
play_rect = play.get_rect()
play_rect.x = 1200
play_rect.y = 25

mot_rect = (100, 200)

game = Game()

running = True
tp = []
Font = pygame.font.SysFont("arial", 20)
i = 0
while running:
    clock.tick(30)

    screen.blit(background, (0, 0))

    if game.is_playing:
        screen.blit(play, play_rect)
        game.update(screen, game)
        if game.score < game.score_fin1:
            background = background1
        elif game.score_fin1 <= game.score < game.score_fin_boss:
            background = background_boss1
        elif game.score < game.score_fin2:
            background = background2
        elif game.score_fin2 <= game.score < game.score_fin_boss2:
            background = background_boss2
        elif game.score < game.score_fin3:
            background = background3
        elif game.score_fin3 <= game.score < game.score_fin_boss3:
            background = background_boss3
        elif game.score == game.score_fin_boss3:
            background = background_fin
        elif game.score == 100:
            game.score = 0

    else:
        screen.blit(play_button, play_button_rect)
        screen.blit(quit_button, quit_button_rect)

    pygame.display.flip()
    for event in pygame.event.get():

        if event.type == pygame.QUIT:
            running = False
            pygame.quit()
            print("game closed")

        elif event.type == pygame.KEYDOWN:
            game.pressed[event.key] = True
            game.do_kill()
        elif event.type == pygame.KEYUP:
            game.pressed[event.key] = False

        elif event.type == pygame.MOUSEBUTTONDOWN:
            if play_button_rect.collidepoint(event.pos):
                # si le bouton est cliquer : lancer le jeux
                game.is_playing = True

            if play_rect.collidepoint(event.pos):
                game.is_playing = False

            if quit_button_rect.collidepoint(event.pos):
                running = False
                pygame.quit()
                print("game close")
