import pygame
import random
import animation
# codée a l'aide de graven developement


class Monster(animation.AnimateSprite):

    def __init__(self, game, name, size, hauteur):
        super().__init__(name, size)
        self.game = game
        self.attack = 10
        self.rect = self.image.get_rect()
        self.hauteur = hauteur
        self.rect.x = 1300
        self.rect.y = self.hauteur
        self.lieu_spawn = 1200
        self.border = 0
        self.velocity = 4
        self.font = pygame.font.SysFont("arial", 20)

    def update_animation(self):
        self.animate()

    def remove(self):
        self.game.all_monster.remove(self)

    def forward(self):
        if not self.game.check_collision(self, self.game.all_player):
            self.rect.x -= self.velocity
            if self.rect.x <= self.border:
                self.respawn()
        else:
            self.game.player.damage(self.attack)
            self.respawn()

    def respawn(self):
        self.rect.y = self.hauteur
        self.rect.x = self.lieu_spawn

    def mot_to_kill(self, surface, mot):
        mtk = self.font.render(mot, False, (255, 255, 255))
        surface.blit(mtk, (self.rect.x - 20, self.rect.y - 20))


class Goblin(Monster):
    def __init__(self, game):
        super().__init__(game, 'goblin', (200, 200), 400)
        self.velocity = random.randint(2, 5)


class Chauvesouris(Monster):

    def __init__(self, game):
        super().__init__(game, 'chauvesouris', (100, 100), 100)
        self.velocity = 3
        if self.rect.x <= self.border:
            self.game.player.damage(self.attack)


class Vere(Monster):

    def __init__(self, game):
        super().__init__(game, 'vere', (100, 100), 450)
        self.velocity = 8


class Burger(Monster):

    def __init__(self, game):
        super().__init__(game, 'burger', (150, 150), 400)


class Boss(Monster):

    def __init__(self, game):
        super().__init__(game, 'boss1', (300, 300), 300)
        self.velocity = 1


class Boss2(Monster):
    def __init__(self, game):
        super().__init__(game, 'boss2', (300, 300), 300)
        self.velocity = 1


class Boss3(Monster):

    def __init__(self, game):
        super().__init__(game, 'boss3', (300, 300), 300)


class Sniker(Monster):

    def __init__(self, game):
        super().__init__(game, 'sniker', (150, 150), 100)


class Twit(Monster):

    def __init__(self, game):
        super().__init__(game, 'twit', (150, 150), 100)


class Neurone(Monster):

    def __init__(self, game):
        super().__init__(game, 'neurone', (200, 200), 400)


class Ramapant(Monster):

    def __init__(self, game):
        super().__init__(game, 'ramapant', (150, 150), 450)
