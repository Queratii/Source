import pygame
# codée à l'aide de  Graven developement

class AnimateSprite(pygame.sprite.Sprite):
    def __init__(self, sprite_name, size=(200, 200)):
        super().__init__()
        self.size = size
        self.image = pygame.image.load(f'asset/{sprite_name}/{sprite_name}_frame1.png')
        self.image = pygame.transform.scale(self.image, size)
        self.curent_image = 0
        self.images = animation.get(sprite_name)

    def animate(self):
        self.curent_image += 1

        if self.curent_image >= len(self.images):
            self.curent_image = 0

        self.image = self.images[self.curent_image]
        self.image = pygame.transform.scale(self.image, self.size)


def load_animation_image(sprite_name):
    images = []
    chemin = f"asset/{sprite_name}/{sprite_name}_frame"
    for num in range(1, 22):
        image_chemin = chemin + str(num) + '.png'
        images.append(pygame.image.load(image_chemin))
    return images


animation = {
    'goblin': load_animation_image('goblin'),
    'chauvesouris': load_animation_image('chauvesouris'),
    'vere': load_animation_image('vere'),
    'boss1': load_animation_image('boss1'),
    'burger': load_animation_image('burger'),
    'boss2': load_animation_image('boss2'),
    'boss3': load_animation_image('boss3'),
    'twit': load_animation_image('twit'),
    'neurone': load_animation_image('neurone'),
    'sniker': load_animation_image('sniker'),
    'ramapant': load_animation_image('ramapant')

}
